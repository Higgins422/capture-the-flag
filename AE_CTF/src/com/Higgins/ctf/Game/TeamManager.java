package com.Higgins.ctf.Game;

import java.util.Random;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.Higgins.ctf.CTF;

public class TeamManager {
	
	public void addRandomTeam(Player p) {
		
		Random r = new Random();
		
		int team = r.nextInt(2) + 1;
		
		if(team == 1) {
			
			if(CTF.red.size() <= CTF.blue.size()) {
				
				CTF.red.add(p);
				p.setPlayerListName("�c" + p.getName());
				p.getInventory().setItem(3, new ItemStack(Material.WOOL, 1, (byte) 14));
				p.sendMessage("�5You joined the �cRed team");
				
			} else {
				
				CTF.blue.add(p);
				p.setPlayerListName("�b" + p.getName());
				p.getInventory().setItem(5, new ItemStack(Material.WOOL, 1, (byte) 11));
				p.sendMessage("�5You joined the �bBlue team");
			}
		}
		
		if(team == 2) {
			
			if(CTF.blue.size() <= CTF.red.size()) {
				
				CTF.blue.add(p);
				p.setPlayerListName("�b" + p.getName());
				p.getInventory().setItem(5, new ItemStack(Material.WOOL, 1, (byte) 11));
				p.sendMessage("�5You joined the �bBlue �5team");
			
			} else {
				
				CTF.red.add(p);
				p.setPlayerListName("�c" + p.getName());
				p.getInventory().setItem(3, new ItemStack(Material.WOOL, 1, (byte) 14));
				p.sendMessage("�5You joined the �cRed �5team");
			}
		}
	}
}
