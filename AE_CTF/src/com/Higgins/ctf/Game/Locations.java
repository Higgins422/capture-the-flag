package com.Higgins.ctf.Game;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class Locations {

	File file = new File("plugins/CTF", "GameLocations.yml");
	FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
	
	public Location getLobby() {
		
		double x = cfg.getDouble("Lobby.x");
		double y = cfg.getDouble("Lobby.y");
		double z = cfg.getDouble("Lobby.z");
		float yaw = cfg.getInt("Lobby.yaw");
		float pitch = cfg.getInt("Lobby.pitch");
		
		Location loc = new Location(Bukkit.getWorld("Lobby"), x, y, z);
		loc.setYaw(yaw);
		loc.setPitch(pitch);
		
		return loc;
	}
	
	public Location getRedSpawn() {
		
		double x = cfg.getDouble("Red.x");
		double y = cfg.getDouble("Red.y");
		double z = cfg.getDouble("Red.z");
		float yaw = cfg.getInt("Red.yaw");
		float pitch = cfg.getInt("Red.pitch");
		
		Location loc = new Location(Bukkit.getWorld("CTF"), x, y, z);
		loc.setYaw(yaw);
		loc.setPitch(pitch);
		
		return loc;
	}
	
	public Location getBlueSpawn() {
		
		double x = cfg.getDouble("Blue.x");
		double y = cfg.getDouble("Blue.y");
		double z = cfg.getDouble("Blue.z");
		float yaw = cfg.getInt("Blue.yaw");
		float pitch = cfg.getInt("Blue.pitch");
		
		Location loc = new Location(Bukkit.getWorld("CTF"), x, y, z);
		loc.setYaw(yaw);
		loc.setPitch(pitch);
		
		return loc;
	}
	
	public Location getRedFlagSpawn() {
		
		double x = cfg.getDouble("RedFlag.x");
		double y = cfg.getDouble("RedFlag.y");
		double z = cfg.getDouble("RedFlag.z");
		float yaw = cfg.getInt("RedFlag.yaw");
		float pitch = cfg.getInt("RedFlag.pitch");
		
		Location loc = new Location(Bukkit.getWorld("CTF"), x, y, z);
		
		return loc;
	}
	
	public Location getBlueFlagSpawn() {
		
		double x = cfg.getDouble("BlueFlag.x");
		double y = cfg.getDouble("BlueFlag.y");
		double z = cfg.getDouble("BlueFlag.z");
		float yaw = cfg.getInt("BlueFlag.yaw");
		float pitch = cfg.getInt("BlueFlag.pitch");
		
		Location loc = new Location(Bukkit.getWorld("CTF"), x, y, z);
		
		return loc;
	}
}
