package com.Higgins.ctf.Game;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Banner;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import com.Higgins.ctf.CTF;
import com.Higgins.ctf.GameState;
import com.Higgins.ctf.Main;

public class FlagCapture implements Listener {
	
	public CTF ctf = new CTF();
	
	@EventHandler
	public void on(PlayerMoveEvent e) {
		Player p = e.getPlayer();
		
		if(CTF.carrying.contains(p)) {
				
			if(p.getLocation().getBlock().getRelative(BlockFace.DOWN).getType() == Material.REDSTONE_BLOCK) {
					
				if(CTF.isOnRedTeam(p) == true) {
					
					CTF.carrying.remove(p);
					
					Bukkit.broadcastMessage(p.getPlayerListName() + " has score for the Red team!");
					
					p.getInventory().setHelmet(null);
					
					CTF.redScore ++;
					
					ctf.placeBlueFlag();
					
					for(Player red : CTF.red) {
						
						red.playSound(red.getLocation(), Sound.LEVEL_UP, 1, 1);
					}
					
					for(Player blue : CTF.blue) {
						
						blue.playSound(blue.getLocation(), Sound.CHICKEN_HURT, 1, 1);
					}
					
					if(CTF.redScore >= 3) {
						
						GameState.setState(GameState.RESTART);
						Main.getInstance().startRestartCountdown();
						Bukkit.broadcastMessage(Main.prefix + "�cCongrats to the Red team for winning!");
						
					}
				}
			}
			
			if(p.getLocation().getBlock().getRelative(BlockFace.DOWN).getType() == Material.LAPIS_BLOCK) {
				
				if(CTF.isOnBlueTeam(p) == true) {
					
					CTF.carrying.remove(p);
					
					Bukkit.broadcastMessage(p.getPlayerListName() + " has score for the Blue team!");
					
					CTF.blueScore ++;
					
					p.getInventory().setHelmet(null);
					
					ctf.placeRedFlag();
					
					for(Player blue : CTF.blue) {
						
						blue.playSound(blue.getLocation(), Sound.LEVEL_UP, 1, 1);
					}
					
					for(Player red : CTF.red) {
						
						red.playSound(red.getLocation(), Sound.CHICKEN_HURT, 1, 1);
					}
					
					if(CTF.blueScore >= 3) {
						
						GameState.setState(GameState.RESTART);
						Main.getInstance().startRestartCountdown();
						Bukkit.broadcastMessage(Main.prefix + "�bCongrats to the Blue team for winning!");
					}
				}
			}
		}
	}
}
