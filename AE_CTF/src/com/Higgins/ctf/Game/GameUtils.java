package com.Higgins.ctf.Game;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;

import com.Higgins.ctf.CTF;
import com.Higgins.ctf.GameState;

public class GameUtils implements Listener {
	
	@EventHandler
	public void on(CreatureSpawnEvent e) {
		
		e.setCancelled(true);
	}
	
	@EventHandler
	public void on(FoodLevelChangeEvent e) {
		
		e.setCancelled(true);
	}
	
	@EventHandler
	public void on(BlockBreakEvent e) {
		
		e.setCancelled(true);
	}
	
	@EventHandler
	public void on(BlockPlaceEvent e) {
		
		e.setCancelled(true);
	}
	
	@EventHandler
	public void on(EntityDamageEvent e) {
		
		if(GameState.getState() == GameState.LOBBY 
				|| GameState.getState() == GameState.RESTART) {
			
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void on(EntityDamageByEntityEvent e) {
		
		if(e.getEntity() instanceof Player && e.getDamager() instanceof Player) {
			
			Player p = (Player) e.getEntity();
			Player d = (Player) e.getDamager();
			
			if((CTF.isOnRedTeam(p) == true && CTF.isOnRedTeam(d) == true) 
					|| CTF.isOnBlueTeam(p) == true && CTF.isOnBlueTeam(d) == true) {
				
				e.setCancelled(true);
				
			}
		}
	}
}
