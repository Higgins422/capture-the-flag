package com.Higgins.ctf.Game.Kits;

import java.util.HashMap;

import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

public class KitClicks implements Listener {

	public static HashMap<Player, String> kits = new HashMap<Player, String>();
	
	@EventHandler
    public void onClick(InventoryClickEvent e) {
        HumanEntity ent = e.getWhoClicked();
        if ((ent instanceof Player)) {
            Player p = (Player) ent;
            if (e.getInventory().getName().equals(KitsMain.getInventory().getName())){
            	e.setCancelled(true);
                ItemStack clicked = e.getCurrentItem();
                
                if (clicked!=null) {
                	//Archer
                    if(clicked.getType() == Material.BOW) {
                    	p.closeInventory();
                    	kits.remove(p);
                    	kits.put(p, "archer");
                    	
                    	p.sendMessage("�5Archer kit selected");
                    }
                    //Warrior
                    else if(clicked.getType() == Material.STONE_SWORD) {
                    	p.closeInventory();
                    	kits.remove(p);
                    	kits.put(p, "warrior");
                    	
                    	p.sendMessage("�5Warrior kit selected");
                    }
                    //Scout
                    else if(clicked.getType() == Material.WOOD_AXE) {
                    	p.closeInventory();
                    	kits.remove(p);
                    	kits.put(p, "scout");
                    	
                    	p.sendMessage("�5Scout kit selected");
                    }
                    //Defender
                    else if(clicked.getType() == Material.IRON_SWORD) {
                    	p.closeInventory();
                    	kits.remove(p);
                    	kits.put(p, "defender");
                    	
                    	p.sendMessage("�5Defender class selected");
                    }
                }
            }
        } 
	}
}
