package com.Higgins.ctf;

import java.io.File;
import java.io.IOException;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import com.Higgins.ctf.Countdowns.LobbyCountdown;

public class Commands implements CommandExecutor {

	File file = new File("plugins/CTF", "GameLocations.yml");
	FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(cmd.getName().equalsIgnoreCase("ctf")) {
			
			if(sender instanceof Player) {
				
				Player p = (Player) sender;
				
				if(p.isOp()) {
					
					double x = p.getLocation().getX();
					double y = p.getLocation().getY();
					double z = p.getLocation().getZ();
					float yaw = p.getLocation().getYaw();
					float pitch = p.getLocation().getPitch();
					
					if(args.length == 0) {
						
						p.sendMessage("§5/ctf setlobby - Set the lobby");
						p.sendMessage("§5/ctf setredspawn - Set the Red team spawn");
						p.sendMessage("§5/ctf setbluespawn - Set the Rlue team spawn");
						p.sendMessage("§5/ctf setredflag - Set the Red team flag");
						p.sendMessage("§5/ctf setblueflag - Set the Blue team flag");
					
					} else {
						
						if(args.length == 1) {
							
							if(args[0].equalsIgnoreCase("startgame")) {
								
								if(GameState.getState() == GameState.LOBBY && !(LobbyCountdown.time > 0)) {
									
									Main.getInstance().startLobbyCountdown();
									
									LobbyCountdown.time = 10;
								
								} else {
									
									p.sendMessage("§cThe game cannot be started at this time");
								}
							}
							
							
							if(args[0].equalsIgnoreCase("setlobby")) {
								
								cfg.set("Lobby.x", x);
								cfg.set("Lobby.y", y);
								cfg.set("Lobby.z", z);
								cfg.set("Lobby.yaw", yaw);
								cfg.set("Lobby.pitch", pitch);
								
								try {
									cfg.save(file);
									p.sendMessage("§aSuccessfully set the lobby!");
								} catch (IOException e) {
									e.printStackTrace();
								}
							}
							
							if(args[0].equalsIgnoreCase("setredspawn")) {
								
								cfg.set("Red.x", x);
								cfg.set("Red.y", y);
								cfg.set("Red.z", z);
								cfg.set("Red.yaw", yaw);
								cfg.set("Red.pitch", pitch);
								
								try {
									cfg.save(file);
									p.sendMessage("§aSuccessfully set the Red team spawn!");
								} catch (IOException e) {
									e.printStackTrace();
								}
							}
							
							if(args[0].equalsIgnoreCase("setbluespawn")) {
								
								cfg.set("Blue.x", x);
								cfg.set("Blue.y", y);
								cfg.set("Blue.z", z);
								cfg.set("Blue.yaw", yaw);
								cfg.set("Blue.pitch", pitch);
								
								try {
									cfg.save(file);
									p.sendMessage("§aSuccessfully set the Blue team spawn!");
								} catch (IOException e) {
									e.printStackTrace();
								}
							}
							
							if(args[0].equalsIgnoreCase("setredflag")) {
								
								cfg.set("RedFlag.x", x);
								cfg.set("RedFlag.y", y);
								cfg.set("RedFlag.z", z);
								cfg.set("RedFlag.yaw", yaw);
								cfg.set("RedFlag.pitch", pitch);
								
								try {
									cfg.save(file);
									p.sendMessage("§aSuccessfully set the Red Flag spawn!");
								} catch (IOException e) {
									e.printStackTrace();
								}
							}
							
							if(args[0].equalsIgnoreCase("setblueflag")) {
								
								cfg.set("BlueFlag.x", x);
								cfg.set("BlueFlag.y", y);
								cfg.set("BlueFlag.z", z);
								cfg.set("BlueFlag.yaw", yaw);
								cfg.set("BlueFlag.pitch", pitch);
								
								try {
									cfg.save(file);
									p.sendMessage("§aSuccessfully set the Blue Flag spawn!");
								} catch (IOException e) {
									e.printStackTrace();
								}
							}
						}
					}
				}
			}
		}
		return true;
	}
}
