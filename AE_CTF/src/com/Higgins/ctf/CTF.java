package com.Higgins.ctf;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.Effect;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Banner;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import com.Higgins.ctf.Game.Locations;

public class CTF {

	public static ArrayList<Player> red = new ArrayList<Player>();
	public static ArrayList<Player> blue = new ArrayList<Player>();
	public static ArrayList<Player> carrying = new ArrayList<Player>();
	public ArrayList<Player> spectator = new ArrayList<Player>();
	
	public static int redScore = 0;
	public static int blueScore = 0;
	
	public Locations loc = new Locations();
	//fixed
	public static boolean isOnRedTeam(Player p) {
		if(red.contains(p)) {
			return true;
		}
		return false;
	}
	
	public static boolean isOnBlueTeam(Player p) {
		if(blue.contains(p)) {
			return true;
		}
		return false;
	}
	
	public static boolean isCarrying(Player p) {
		if(carrying.contains(p)) {
			return true;
		}
		return false;
	}
	
	public void placeRedFlag() {
		
		Block red = Bukkit.getWorld("CTF").getBlockAt(loc.getRedFlagSpawn());
		
		red.setType(Material.STANDING_BANNER);
		
		Banner flag = (Banner) red.getState();
		flag.setBaseColor(DyeColor.RED);
		flag.update();
		
		flag.getWorld().playEffect(flag.getLocation(), Effect.FLAME, 1);
	}
	
	public void placeBlueFlag() {
		
		Block blue = Bukkit.getWorld("CTF").getBlockAt(loc.getBlueFlagSpawn());
		
		blue.setType(Material.STANDING_BANNER);
		
		Banner flag = (Banner) blue.getState();
		flag.setBaseColor(DyeColor.BLUE);
		flag.update();
		
		flag.getWorld().playEffect(flag.getLocation(), Effect.FLAME, 1);
	}
	
	public void makeSpectator(Player p) {
		
		spectator.add(p);
		p.setGameMode(GameMode.SPECTATOR);
		
		for(Player pl : Bukkit.getOnlinePlayers()) {
			
			pl.hidePlayer(p);
		}
		
		for(Player specs : spectator) {
			
			specs.hidePlayer(p);
			p.hidePlayer(specs);
		}
		
		p.sendMessage("�5You are now spectating...");
	}
	
	public static void giveContents(Player p) {
		
		ItemStack helm = new ItemStack(Material.LEATHER_HELMET);
		ItemStack chest = new ItemStack(Material.LEATHER_CHESTPLATE);
		ItemStack legs = new ItemStack(Material.LEATHER_LEGGINGS);
		ItemStack boots = new ItemStack(Material.LEATHER_BOOTS);
		
		LeatherArmorMeta hMeta = (LeatherArmorMeta) helm.getItemMeta();
		LeatherArmorMeta cMeta = (LeatherArmorMeta) chest.getItemMeta();
		LeatherArmorMeta lMeta = (LeatherArmorMeta) legs.getItemMeta();
		LeatherArmorMeta bMeta = (LeatherArmorMeta) boots.getItemMeta();
		
		if(isOnRedTeam(p) == true) {
			
			hMeta.setColor(Color.RED);
			cMeta.setColor(Color.RED);
			lMeta.setColor(Color.RED);
			bMeta.setColor(Color.RED);
			
			helm.setItemMeta(hMeta);
			chest.setItemMeta(cMeta);
			legs.setItemMeta(lMeta);
			boots.setItemMeta(bMeta);
		}
		
		else if(isOnBlueTeam(p) == true) {
			
			hMeta.setColor(Color.BLUE);
			cMeta.setColor(Color.BLUE);
			lMeta.setColor(Color.BLUE);
			bMeta.setColor(Color.BLUE);
			
			helm.setItemMeta(hMeta);
			chest.setItemMeta(cMeta);
			legs.setItemMeta(lMeta);
			boots.setItemMeta(bMeta);
		}
		
		p.getInventory().setHelmet(helm);
		p.getInventory().setChestplate(chest);
		p.getInventory().setLeggings(legs);
		p.getInventory().setBoots(boots);
	}
}
